FROM alpine
RUN apk add wpa_supplicant openssh-keygen bash dialog
COPY *.sh /ctx/
RUN chmod +x /ctx/*.sh
CMD ["/ctx/entrypoint.sh"]
