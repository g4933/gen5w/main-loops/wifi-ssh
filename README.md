# Wifi + SSH

This is one of the `main_loop` projects aimed at adding functionality on a feature by feature basis. The project contains the code needed to enable Wifi and SSH in your unit on a session by session basis.

## How to build the image?
*You will need the SSID and password of the wireless network you want the car to connect to*

Clone this project and run:
```bash
docker build -t gen5wsshloop ./
```

## How to patch the update?

Go to the folder where you have the project cloned and run:
```bash
docker run --rm -it -v $PWD:/mnt gen5wsshloop
```
And just follow the prompts. 

You will end up with a folder named `USB_PACKAGE`; copy its contents to the root of your USB drive and plug into your car.

## How to SSH into your car
TBA
