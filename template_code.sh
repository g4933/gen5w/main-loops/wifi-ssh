#!/bin/bash

# Changes done here are lost after reboot;
# Thanks Javi for making this possible!


USB_PATH=$(dirname $0)
cd $USB_PATH

printf "\n----- Caller:($(ps -o comm= $PPID)) - PID:($PPID) - At($(date)) -----\n" ;


# shut down all networking + iptables, this should disable cell data and prevent abuse
systemctl stop supplicantmanager ;
systemctl stop iptables-manager ;
systemctl stop connman ;

# connect to wifi
/usr/sbin/iptables -P INPUT ACCEPT ;
/usr/bin/wpa_passphrase <REPLACEWITHSSID> "<REPLACEWITHPSK>" > /tmp/wpa.conf ;
/usr/sbin/wpa_supplicant -B -i wlan0 -c /tmp/wpa.conf >> $USB_PATH/wpa_supplicant.log 2>&1 &

# A bit of wait to give it time to process stuff
echo "Wait 15 secs for AP association..."
sleep 15

### IP ROUTING CONFIG ###
## setup IP address according to your LAN, default gw left as exercise
udhcpc -i wlan0
### IP ROUTING CONFIG ###

# setup dropbear
mkdir -p /tmp/dropbear ;
/usr/sbin/dropbearkey -t rsa -f /tmp/dropbear/dropbear_rsa_host_key ;
mkdir -p /home/root/.ssh ;
chmod 700 /home/root/.ssh ;

#### PUT YOUR authorized_keys in /home/root/.ssh/authorized_keys
mkdir -p ./ssh # Ensuring this folder exists in the USB
touch ./ssh/SSH_KEYS_GO_HERE # Adding a file in case the user has troubles identifying here to put his pub keys
cat ./ssh/id_rsa.pub > /home/root/.ssh/authorized_keys ; 
chmod 600 /home/root/.ssh/authorized_keys ; 

# dropbear will check this file for valid shells before auth
echo /bin/sh >> /tmp/shells ;

# run dropbear on port 2002, no fork, send logs to stdout, disallow pw login, etc. Not sending to BG intentionally, so it prevents the loop from going on.
/usr/sbin/dropbear -F -E -p 2002 -s -g -r /tmp/dropbear/dropbear_rsa_host_key

while true
do 
    echo "looping... $PPID"
    /sbin/ip addr show dev wlan0 
    /sbin/ip link show dev wlan0
    /sbin/ifconfig
    sleep 30;
done


printf "\n----- END Caller:($(ps -o comm= $PPID)) - PID:($PPID) - At($(date)) END -----\n" ;
